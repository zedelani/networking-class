﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class SimpleUdpSrvr
{
    public static void Main()
    {
        int recv;
        string input, uname, message;
        byte[] data = new byte[1024];
        IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 9050);

        Socket newsock = new Socket(AddressFamily.InterNetwork,
                        SocketType.Dgram, ProtocolType.Udp);

        newsock.Bind(ipep);
        Console.WriteLine("Waiting for a client...");

        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        EndPoint Remote = (EndPoint)sender;

        recv = newsock.ReceiveFrom(data, ref Remote);
        uname = Encoding.ASCII.GetString(data, 0, recv);
        Console.WriteLine("Client Uname : " + uname);

        Console.Write("Type your uname : ");
        string welcome = Console.ReadLine();
        data = Encoding.ASCII.GetBytes(welcome);
        newsock.SendTo(data, data.Length, SocketFlags.None, Remote);

        while (true)
        {
            data = new byte[1024];
            recv = newsock.ReceiveFrom(data, ref Remote);
            message = Encoding.ASCII.GetString(data, 0, recv);
            Console.WriteLine(uname + " : " + message);
            Console.Write(welcome + " : ");
            input = Console.ReadLine();
            newsock.SendTo(Encoding.ASCII.GetBytes(input), Remote);
        }
    }
}