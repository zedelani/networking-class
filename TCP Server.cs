﻿// created on 9/21/2001 at 10:59 AM
using System;
using System.Net.Sockets;
using System.IO;
public class ServerSocket1
{
    public static void Main()
    {
        try
        {
            bool status = true;
            string servermessage = "";
            string clientmessage = "";
            string clientuname = "";
            string serveruname = "";
            TcpListener tcpListener = new TcpListener(8100);
            tcpListener.Start();
            Console.WriteLine("Server Started");
            Socket socketForClient = tcpListener.AcceptSocket();
            Console.WriteLine("Client Connected");
            NetworkStream networkStream = new NetworkStream(socketForClient);
            StreamWriter streamwriter = new StreamWriter(networkStream);
            StreamReader streamreader = new StreamReader(networkStream);
      
            servermessage = streamreader.ReadLine();
            clientuname = servermessage;
            Console.WriteLine(clientuname + " is Sign In!" );

            Console.Write("Input Your Name : ");
            serveruname = Console.ReadLine();
            streamwriter.WriteLine(serveruname);
            streamwriter.Flush();


            while (status)
            {
           
                if (socketForClient.Connected)
                {
                    servermessage = streamreader.ReadLine();
                    Console.WriteLine( clientuname + " : " + servermessage);
                    if ((servermessage == "bye" ) || (servermessage == "BYE") || (clientmessage == "bye") || (clientmessage == "BYE"))
                    {
                        status = false;
                        streamreader.Close();
                        networkStream.Close();
                        streamwriter.Close();
                        return;
                    }
                    Console.Write(serveruname + " : " );
                    clientmessage = Console.ReadLine();
                    streamwriter.WriteLine(clientmessage);
                    streamwriter.Flush(); 
                }
            }
            streamreader.Close();
            networkStream.Close();
            streamwriter.Close();
            socketForClient.Close();
            Console.WriteLine("Exiting");
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
}
// Source Code Ends 